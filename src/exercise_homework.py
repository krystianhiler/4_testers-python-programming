def describe_player(player_dictionary):
    nick = player_dictionary["nick"]
    player_type = player_dictionary["type"]
    exp_points = player_dictionary["exp_points"]
    print(f"The player {nick} is of type {player_type} and has {exp_points} EXP")


if __name__ == '__main__':
    player = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }
    describe_player(player)

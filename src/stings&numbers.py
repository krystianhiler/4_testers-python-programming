first_name = "Krystian"
last_name = "Hiler"
email = "krystian.hiler@gmail.com"

# F-strings
my_bio_using_f_string = f"Mam na imię {first_name}.\nMoje nazwisko to {last_name}.\nMój email to {email}."
print(my_bio_using_f_string)

#Algebra#
circle_radius = 7
area_of_a_circle_with_radius_7 = 3.14 * 7 **2
print(area_of_a_circle_with_radius_7)
circumference_of_a_circle_with_radius_7 = 2 * 3.14 * circle_radius
print(circumference_of_a_circle_with_radius_7)


print(f"Area of a circle with radius {circle_radius}:", area_of_a_circle_with_radius_7)
print(f"Circumference of a circle with radius {circle_radius}:", circumference_of_a_circle_with_radius_7)

# Zadanie domowe
def convert_temps_to_fahrenheit(temps_celsius):
    temps_fahrenheit = []
    for temp_celsius in temps_celsius:
        temp_fahrenheit = (temp_celsius * 9 / 5) + 32
        temps_fahrenheit.append(temp_fahrenheit)
    return temps_fahrenheit


temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
temps_fahrenheit = convert_temps_to_fahrenheit(temps_celsius)
print(temps_fahrenheit)



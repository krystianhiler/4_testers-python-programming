import random
import datetime


def generate_person(gender):
    female_first_names = ["Kate", "Agnieszka", "Anna", "Maria", "Joss", "Eryka"]
    male_first_names = ["James", "Bob", "Jan", "Hans", "Orestes", "Saturnin"]
    last_names = ["Smith", "Kowalski", "Yu", "Bona", "Muster", "Skinner", "Cox", "Brick", "Malina"]
    countries = ["Poland", "United Kingdom", "Germany", "France", "Other"]

    if gender == "male":
        first_name = random.choice(male_first_names)
    else:
        first_name = random.choice(female_first_names)

    last_name = random.choice(last_names)
    country = random.choice(countries)
    email = f'{first_name.lower()}.{last_name.lower()}@example.com'
    age = random.randint(5, 45)
    adult = age >= 18
    current_year = datetime.datetime.now().year
    birth_year = current_year - age
    person = {
        'first_name': first_name,
        'last_name': last_name,
        'country': country,
        'email': email,
        'age': age,
        'adult': adult,
        'birth_year': birth_year
    }
    return person


if __name__ == '__main__':

    people = []
    for male in range(5):
        person = generate_person("male")
        people.append(person)
    for female in range(5):
        person = generate_person("female")
        people.append(person)
    for person in people:
        print(
            f"Hi! I'm {person['first_name']} {person['last_name']}. I come from {person['country']} and I was born in {person['birth_year']}.")

def display_speed_info(speed):
    if speed > 50:
        print("Slow down!")
    else:
        print("Thank you, your speed is below limit")

    # Zadanie


def check_normal_conditions(temperature, pressure):
    if temperature == 0 and pressure == 1013:
        return True
    else:
        return False


print(check_normal_conditions(0, 1013))
print(check_normal_conditions(1, 1013))
print(check_normal_conditions(0, 1014))
print(check_normal_conditions(1, 1014))


# Zadanie
def grade_description(grade):
    if grade >= 4.5:
        return "bardzo dobry"
    elif grade >= 4.0:
        return "dobry"
    elif grade >= 3.0:
        return "dostateczny"
    elif grade >= 2.0:
        return "niedostateczny"
    else:
        return "N/A"
    


print(grade_description(4.5))
print(grade_description(4.0))
print(grade_description(3.5))
print(grade_description(2.5))
print(grade_description(1.5))

shopping_list = ['bananas', 'chicken', 'tomatoes', 'sponge', 'water']
print(shopping_list[0])
print(shopping_list[2])
print(shopping_list[-3])
shopping_list.append('cucambers')
print(shopping_list)

number_of_items_to_buy = len(shopping_list)
print(number_of_items_to_buy)

first_two_shopping_items = shopping_list[0:2]
print(first_two_shopping_items)

animal = {
    "name": "Stefan",
    "kind": "cat",
    "age": 12,
    "male": True
}
cat_age = animal["age"]
print("Cat age:", cat_age)
cat_male = animal["male"]
print("Cat male:", cat_male)

animal["age"] = 6
print(animal)
animal["owner"] = "Lucjan"
print(animal)

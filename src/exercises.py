def calculate_second_power_of_number(input_number):
    return input_number ** 2


if __name__ == '__main__':
    # Zadanie 1
    print(calculate_second_power_of_number(0))
    print(calculate_second_power_of_number(16))
    print(calculate_second_power_of_number(2.55))

def square(x):
    return x * x

square_0 = square(0)
square_16 = square(16)
square_2_55 = square(2.55)

print(square_0, square_2_55, square_16)

    #Zadanie 2


def celsius_to_fahrenheit(degrees_c):
    return degrees_c * 9 / 5 + 32


fahrenheit = celsius_to_fahrenheit(20)
print(fahrenheit)

    #Zadanie 3


def rectangular_parallelepiped_volume(a, b, c):
    return a * b * c

volume = rectangular_parallelepiped_volume(3, 5, 7)
print(volume)
